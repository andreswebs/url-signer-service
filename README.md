# url-signer-service

(Proof of Concept)

## Configuration

Set the following variables on Gitlab:

**Variables:**

AWS_CREDENTIALS (file) - credentials file

AWS_ACCOUNT - account number

AWS_REGION - region

S3_BUCKET - bucket name for the signed URL

## Usage

`curl <service address>/url?o=<file name>`