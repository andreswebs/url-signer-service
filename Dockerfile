FROM node:14-alpine AS base

RUN \
  mkdir /app && \
  chown -R node:node /app

COPY --chown=node:node ./app/* /app/

WORKDIR /app

USER node

RUN yarn install \
  --frozen-lockfile \
  --production \
  --silent \
  --non-interactive

EXPOSE 3000

CMD [ "node", "/app/server.js" ]
