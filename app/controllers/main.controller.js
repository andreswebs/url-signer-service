require('dotenv').config();

const AWS = require('aws-sdk');
const createError = require('http-errors');
const { v4: uuidv4 } = require('uuid');

const s3 = new AWS.S3({
  signatureVersion: 'v4',
});

const Bucket = process.env.S3_BUCKET;

function getSignedUrl(req, res, next) {

  const o = req.query.o;
  const actionId = uuidv4();
  const Key = `${o}.${actionId}.txt`;

  if (!Bucket) {
    const error = new Error('missing s3 bucket name');
    return next(createError(500, error, { expose: false }));
  }

  const s3Params = {
    Bucket,
    Key,
    ContentType: 'text/plain',
    ACL: 'public-read',
    Expires: 3600
  };

  const url = s3.getSignedUrl('putObject', s3Params);

  return res.json({
    file: Key,
    url
  });

}


module.exports = {
  getSignedUrl
};