const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;

const server = require('../server');

chai.use(chaiHttp);

// all keys
const all = [
  'file',
  'url'
];

describe('url-signer-service', function () {

  //disable timeout
  this.timeout(0);

  context('GET /url', function () {

    it('should get a signed url and file name', function (done) {

      const q = '?o=test';

      chai.request(server)
        .get(`/url${q}`)
        .end((err, res) => {
          expect(res.status).to.equal(200);
          expect(res.body).to.be.an('object');
          expect(res.body).to.include.keys(all);
          done();
        });

    });

  });

});