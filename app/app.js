const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const logger = require('morgan');

const notFound = require('./middleware/not-found');
const errorHandler = require('./middleware/error-handler');

const router = require('./routes/main.router');

const app = express();

app.use(helmet());
app.use(cors());
app.use(logger('dev'));

app.use(router);

// catch 404 and forward to error handler
app.use(notFound);

// error handler
app.use(errorHandler);

module.exports = app;
