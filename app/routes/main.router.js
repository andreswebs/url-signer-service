const express = require('express');
const router = express.Router();

const controller = require('../controllers/main.controller');

router.route('/url').get(controller.getSignedUrl);

module.exports = router;
